import requests
import json


class BaseConnector():
    def __init__(self, host, creds, _print):
        self._host = host
        self._creds = creds
        self._print = _print
        self.default_headers = {"Content-Type": "application/json"}

    def _query(self, url, method="get", json_data=None):
        if method not in ['get', 'post', 'delete', 'patch', 'put']:
            raise AttributeError("(CND) Method not allowed")

        full_url = f"{self._host}{url}"
        self.kwargs = {}
        if json_data is not None:
            self.kwargs["json"]=json_data
        response = getattr(requests, method)(
            full_url,
            headers=self.default_headers,
            auth=(self._creds["username"], self._creds["password"]),
            verify=False,
            **self.kwargs
        )
        if response.status_code in [200, 201, 202, 404]:
            return {
                "content": json.loads(response.content), 
                "status_code": response.status_code
            }
        return False

    def get_range_ips(self, mask, slash, limit):
        pass

    def reserve_ip(self, hostname, ip):
        pass

    def _get_ip_info(self, ip):
        pass

    def release_ip(self, ip):
        pass