import pkg_resources


path = pkg_resources.resource_filename("cndip", "VERSION")
__version__ = open(path).read()
