from cndip._base_connector import BaseConnector


class SolarWindConnector(BaseConnector):
    lock_key = "vRA Owned"

    def connect(self):
        # how should I connect to Solard ?
        pass

    def get_range_ips(self, mask, slash, limit):
        # replace with call api to Solarwind
        result = self._query('url')
        ips = result["content"]
        return ips

    def reserve_ip(self, hostname, ip):
        # Call API and add self.lock_key as comment
        result = self._query('url', method="post")
        if result["status_code"] in [200, 201]:
            return True
        else:
            return False

    def _get_ip_info(self, ip):
        # Call API and delete only if self.lock_key are in comment
        result = self._query('url')
        if result["status_code"] in [200, 201]:
            return result["content"]
        else:
            return None 

    def release_ip(self, ip):
        ip_info = self._get_ip_info(ip)
        if self.lock_key not in ip_info["comment"]:
            return None
        result = self._query('url', method="delete")
        if result["status_code"] in [200, 201]:
            return True
        else:
            return False    