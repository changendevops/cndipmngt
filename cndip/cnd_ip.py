import requests
import json


class CndIp():
    def __init__(self, vra8, connector, hostname, _print):
        self.vra8 = vra8
        self.connector = connector
        self.hostname = hostname
        self._print = _print

    def select_ip(ip_adresses):
        ## here a method to select witch one you want
        return ip_adresses[0]

    def reserve_ip(self, network_id):
        network_info = self.vra8.get_network_info(network_id)
        ip_addresses = self.connector.get_range_ips(network_info["mask"], network_info["slash"], 20)
        ip_address = self.select_ip(ip_addresses)

        return self.connector.reserve_ip(self.hostname, ip_address)

    def release_ip(self, ip):
        ## if we add additional task... cal it from here
        return self.connector.release_ip(ip)