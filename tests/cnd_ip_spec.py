import sys
sys.path.append('lib')
sys.path.append('cndip')
from mockito import when, mock, unstub
from expects import *
from mamba import description, context, it
import yaml, json
import cndprint
import cndip
# import cndvra8
import requests


_print = cndprint.CndPrint(level="Trace")
hostname = 'mon_hostname.com'


with description('Base') as self:
    with before.each:
        unstub()
        self.cndip = cndip.cnd_ip.CndIp(vra8, connector, hostname, _print)

    with context("__init__"):
        with it("shoud set the vra8, connector, hostname and print"):
            expect(self.cndip.vra8).to(equal(vra8))
            expect(self.cndip.connector).to(equal(connector))
            expect(self.cndip.hostname).to(equal(hostname))
            expect(self.cndip._print).to(equal(_print))

    with context("reserve_ip"):
        with before.each:
            when(self.cndip).vra8(...).thenReturn(vra8)

        with it("shoud reserve IP"):
            when(self.cndip).select_ip(...).thenReturn('127.0.0.1')
            result = self.cndip.reserve_ip('agssfs-aaaqdd-98jssss')
            expect(result).to(equal(True))