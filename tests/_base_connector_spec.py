import sys
sys.path.append('lib')
sys.path.append('cndip')
from mockito import when, mock, unstub
from expects import *
from mamba import description, context, it
import yaml, json
import cndip._base_connector
import cndprint
import requests


_print = cndprint.CndPrint(level="Trace")
host = 'https://sample.com/'
creds = {'username': 'whoami', 'password': 'shuuuuut!'}

with description('Base') as self:
    with before.each:
        unstub()
        self.base = cndip._base_connector.BaseConnector(host, creds, _print)

    with context("__init__"):
        with it("shoud set the host, creds and print class"):
            expect(self.base._host).to(equal(host))    

    with context("_query"):
        with it("shoud accept get by default and load the json"):
            response = mock({'content': '{"a": "b"}', 'status_code': 200}, spec=requests.Response)
            when(requests).get(...).thenReturn(response)
            result = self.base._query('url')
            expect(result["content"]).to(equal({"a": "b"}))

    with context("_query"):
        with it("shoud return False if result is not good"):
            response = mock({'content': '{"a": "b"}', 'status_code': 500}, spec=requests.Response)
            when(requests).get(...).thenReturn(response)
            result = self.base._query('url')
            expect(result).to(equal(False))                